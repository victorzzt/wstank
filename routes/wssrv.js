// WebSocketServer handler, handles game connection, currently not enclosed in class
var expiry = 7200000;
var WebSocketServer = require('websocket').server;
var debug = require('debug')('wstank:server');
var WSconnections = []; // maintain a list
var srv_setting = {
    'type':'srvcfg',
    'sync_interval':40 // ms
}

var active_pid = [];
var sync_dat = {
    'pid'  :[],     // synced pid list for using native index matching
    'tanks':[],     // for players
    'bullets':[],   // for bullets
    'evt':[]        // event (create, destroy, etc)
};

var template_tank = {   // for short transmit the names are short / may not need
    'id': 0,
    'x': 0,
    'y': 0,
    'vx': 0,
    'vy': 0
}

var sendQueue = []; // queue to send

module.exports = function(server){   // lazy export
    wsServer = new WebSocketServer({
        httpServer: server,
        autoAcceptConnections: true // insecure option, however prevent from most errors
    });
    wsServer.on('connect', onconnect );
}

var serverBCast = setInterval(function(){   // for queued sending
    WSconnections.forEach(function(connection){
        if( sendQueue.length > 0 ){ // no point sending empty ones
            // right now broadcast everything, and ignore self on client
            connection.sendUTF(JSON.stringify(sendQueue));
        }
    });
    
    sendQueue = []; // clear it ( is it inefficient because it's mallocing and freeing? )
}, srv_setting.sync_interval);

function onconnect(connection){  
    debug((new Date()) + ' Peer ' + connection.remoteAddress + ' Connected');
    
    WSconnections.push(connection); // add to list
    let sessionparam = {};              // This session's Player ID (need to pass object to affect the outside)
    sessionparam.pid = -1;
    
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            // Translate into JSON object
            let msgjso = {};
            try{
                msgjso = JSON.parse(message.utf8Data);
            }catch(e){
                // no need to know, just process raw string (message.utf8Data)
            }
            // Handle Array
            if( Array.isArray(msgjso) ){
                if( msgjso.length > 0 ){
                    msgjso.forEach( function( element ){
                        handlePinQ(connection , element, sessionparam);
                    } );
                }
            }
            
            // Handle Ping
            if( msgjso.type == 'ping' ){
                handleping(connection, msgjso, sessionparam);
            }
            
            // Handle Player
            if( msgjso.type == 'preq' ){
                handleplayerRequest(connection, msgjso, sessionparam);
            }
        }
        else if (message.type === 'binary') {
            debug(' Peer ' + connection.remoteAddress + ' Received Binary Message of ' + message.binaryData.length + ' bytes');
            // connection.sendBytes(message.binaryData);
        }
    });
    
    connection.on('close', function(reasonCode, description) {
        let rmv_idx = WSconnections.indexOf(connection);    // Remove the connection from pool
        if( rmv_idx != -1 ){    // error protector
            WSconnections.splice(rmv_idx, 1);
            debug((new Date()) + ' Peer No:' + rmv_idx + "@" + connection.remoteAddress + ' disconnected.');
        }else{
            debug((new Date()) + ' Peer not in list, how weird!');
        }
        
        if( sessionparam.pid != -1 ){   // Deactivate Player ID
            // Kill for everyone else
            let bcast_msg = {'type':'pdel', 'pid':sessionparam.pid};  // only need to know pid to kill
            WSconnections.forEach(function(conn){
                let bcast_str = JSON.stringify(bcast_msg);
                if(conn != connection){ // no need to send to self
                    conn.sendUTF(bcast_str);
                }
            });
            
            // active one
            let act_idx = active_pid.indexOf(sessionparam.pid);
            if( act_idx == -1 ){
                debug("Not inside active, something is wrong: "+sessionparam.pid);
            }else{
                active_pid.splice(act_idx, 1);  // delete it
                debug("Now active: ", active_pid);
            }
        }
    });
}

// packet
function handlePinQ(connection, msgjso, sessionparam){
    // There shouldn't be preq or ping
    if( msgjso.type == 'pupd' ){
        // push into bcast queue
        /*
        let existing = searchobjarr(sendQueue, 'pid', msgjso.pid);
        if( existing ){ // means haven't sent and got new one, override (seems discarding is producing some problem)
            sendQueue.splice( sendQueue.indexOf(existing), 1 );
        }*/
        sendQueue.push(replicate(msgjso));  // to be safe(inefficient?)
    }
}

// refactored internal callbacks, putting here as later might be reused in processing message in a queue
function handleping(connection, msgjso, sessionparam){    // handles ping
    msgjso.type = 'pong';
    let msgrpl = JSON.stringify(msgjso);
    connection.sendUTF(msgrpl);
    
    // also update expiry
    if( sessionparam.pid != -1 ){
        let this_tank = searchobjarr(sync_dat.tanks, 'pid', sessionparam.pid);
        let now = (new Date()).getTime();
        this_tank.expiry = now + expiry;   // refresh expiry
    }
}

function handleplayerRequest(connection, msgjso, sessionparam){
    // Request player
    debug(msgjso);
    let isactive = false;
    if( msgjso.pid == -1 || sync_dat.pid.indexOf(msgjso.pid) == -1){    // either no pid or pid isn't in list
        do{
            msgjso.pid = playerid();
        }while( sync_dat.pid.indexOf(msgjso.pid) != -1 )    // although very unlikely do not want to clash with other pids
        debug("new player, assigned id:" + msgjso.pid);
        // Give expiry time
        let now = (new Date()).getTime();
        msgjso.expiry = now + expiry;
        msgjso.tint = HSVtoRGB(Math.random(),0.75,1.0);
        sync_dat.pid.push(msgjso.pid);
        sync_dat.tanks.push(msgjso);
    }else{  // Existing
        if( active_pid.indexOf(msgjso.pid) != -1 ){    // this is active
            isactive = true;
        }
        let idx_load = sync_dat.pid.indexOf(msgjso.pid);
        let saved_tank = sync_dat.tanks[idx_load];
        msgjso.tint = saved_tank.tint;  // load tint
        let now = (new Date()).getTime();
        msgjso.expiry = now + expiry;   // refresh expiry
    }
    msgjso.type = 'prpl';
    // let's echo the msg back
    if( !isactive){ // halt send if already opened
        // Need to push a full sync
        let enemies = getOtherTanks(connection, msgjso);    // getother tanks also send me to everyone else
        connection.sendUTF(JSON.stringify(enemies));
        // querying tanks before pushing into active
        sessionparam.pid = msgjso.pid;    // mark this session's pid
        active_pid.push(sessionparam.pid);
        connection.sendUTF(JSON.stringify(msgjso));
        connection.sendUTF(JSON.stringify(srv_setting));
    }else{
        debug("Duplicate session rejected: ", msgjso.pid);
        connection.sendUTF(JSON.stringify({'type':'reject','err':'duplicate'}));
    }
    debug("Now active: ", active_pid);
}

function getOtherTanks(connection, msgjso){
    let result = [];
    let me = replicate(msgjso);
    me.type = 'pspw';   // player spawn
    let me_str = JSON.stringify(me);
    
    debug(me_str);
    
    WSconnections.forEach(function(conn){
        if(conn != connection){ // not me (player spawn not queued, instant send)
            conn.sendUTF(me_str);
        }
    });
    
    // Find active that's not me (because it's not pushed yet, so should be OK)
    active_pid.forEach(function(pid){
        // still protect
        if(pid != me.pid){
            let enemyobj = searchobjarr(sync_dat.tanks, 'pid', pid);
            let enemycpy = replicate(enemyobj);
            enemycpy.expiry = undefined;    // no need to send, mute it
            enemycpy.type = 'pspw';
            result.push(enemycpy);
        }
    });

    return result;
}

// internally used functions
function playerid() {
  // copied from stackoverflow
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 4 characters
  // after the decimal.
  return Math.random().toString(36).substr(2, 4);
};

function HSVtoRGB(h, s, v) {
    // copied from stackoverflow
    let r, g, b, i, f, p, q, t;
    if (arguments.length === 1) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    r = Math.round(r * 255)
    g = Math.round(g * 255)
    b = Math.round(b * 255)
    
    // modified for tint
    return r << 16 | g << 8 | b;
}

function replicate(obj){    // inefficient, copy object here so that we don't modify the original
    return JSON.parse(JSON.stringify(obj));
}

function searchobjarr(arr, field, val){
    for( let i = 0; i < arr.length; i++ ){
        if( arr[i][field] == val ){
            return arr[i];
        }
    }
    return undefined;
}