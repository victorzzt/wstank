function startGame(){
    // Network
    var wso = new wsgame();

    // Network is global
    wso.connect("", true);

    var game = new Phaser.Game(phaserconfig);
    // pass the wso inside config as an additional member
    // I do not know if it's a good practise
    // The network object is accessable by all scenes, need to handle remove handler when destroying scene
    game.wso = wso;
}