class tank extends Phaser.GameObjects.Container{
    constructor(scene, x, y, children) {
        if( children == undefined ){
            children = [];
        }

        let tankbot = scene.add.sprite(0,0,'tankbot').play('run');
        tankbot.setAngle(180);  // here also same
        let tanktur = scene.add.image(0,0,'turret');
        children.push(tankbot);
        children.push(tanktur);
        tanktur.setAngle(180);
        
        super(scene, x, y, children);
        this.scene = scene;
        
        // Need to give some labels to be identified in collision
        this.setData('type', 'tank');
        
        // for easy access
        this.tankbot = tankbot;
        this.tanktur = tanktur;
        
        scene.add.existing(this);
        this.tankbot.anims.pause();
        
        // add physics
        this.setSize(64, 64);
        this.physicsContainer = scene.matter.add.gameObject(this);
        this.physicsContainer.setFrictionAir(0.2);
        this.physicsContainer.setBounce(0.9);
        this.physicsContainer.setFixedRotation(true);
        
        this.stop();
        
        // move speed
        this.moveForce = 0.010;
        
        // left right
        this.lastFired = 0;
        
        // enable physice
        this.unmuteMatter();
        
        // need to have an initial dir to prevent from undefined
        this.mvdir = 0;
        this.turdeg = 0;
    }
    
    static loadsprites(scene){ // class method ?
        scene.load.animation('tankbotani', 'assets/tank/tankbotani.json');
        scene.load.atlas('tankbot', 'assets/tank/tankbot.png', 'assets/tank/tankbot.json');
        
        scene.load.image('turret', 'assets/tank/tankturret.png');
        
        bullet.loadsprites(scene);
    }
    
    stop(){
        // right now has friction so no need to do anything
        this.tankbot.anims.pause();
    }
    
    run(dir){
        this.mvdir = dir;
        if( dir == 0 ){
            this.stop();    // stop
            return;
        }
        
        this.tankbot.anims.resume();
        /*
        let forceX = 0;
        let forceY = 0;
        if( dir & dirMask.left ){
            forceX = -this.moveForce;
        }
        if( dir & dirMask.right){
            forceX = this.moveForce;
        }
        if( dir & dirMask.up ){
            forceY = -this.moveForce;
        }
        if( dir & dirMask.down ){
            forceY = this.moveForce;
        }
        */
        
        // rotation
        let rot = this.angle;
        let rot_target = dirTranslate[dir];
        if( rot_target != -1 ){
            // Lesson learned: MatterJS's default 0 degree is actually facing right
            // Need to read the document instead of guessing, fortunately CW is correct guess
            // Doc location: https://photonstorm.github.io/phaser3-docs/Phaser.Physics.Matter.Sprite.html#angle__anchor
            this.setAngle(rot_target - 180);
        }
        
        // this.physicsContainer.applyForceFrom(new Phaser.Math.Vector2(32,32),new Phaser.Math.Vector2(forceX, forceY));
        this.physicsContainer.thrust(this.moveForce);
    }
    
    point(dir){ // this dir is in deg
        if( dir.x == this.x && dir.y == this.y ){
            return; // mouse is on the center, can't tell angle
        }
        let vec_dir = new Phaser.Math.Vector2( dir.x - this.x, dir.y - this.y );
        let facingDeg = rad2deg(vec_dir.angle());   // using degree for consistancy, not necessary
        
        this.pointang(facingDeg);
    }
    
    pointang(deg){
        if( deg == undefined ){
            return;
        }
        this.tanktur.setAngle(deg - this.angle + 180);  // plus 180 as the sprite is reversed
        this.turdeg = deg;
    }
    
    setTint(col){
        // clear them firstChild
        this.tankbot.clearTint();
        this.tanktur.clearTint();
        
        this.tankbot.setTint(col, col, col, col);
        this.tanktur.setTint(col, col, col, col);
    }
    
    muteMatter(){
        this.physicsContainer.setCollisionCategory(this.scene.recyclematter);
        this.physicsContainer.setCollidesWith(this.scene.recyclematter)
    }
    
    unmuteMatter(){
        this.physicsContainer.setCollisionCategory(this.scene.activematter);
        this.physicsContainer.setCollidesWith([this.scene.activematter, this.scene.worldobj]);
    }
    
    live(){
        this.setActive(true);
        this.setVisible(true);
        this.unmuteMatter();
    }
    
    die(){
        this.setActive(false);
        this.setVisible(false);
        this.muteMatter();
        this.x = 0;
        this.y = 0;
    }
 }