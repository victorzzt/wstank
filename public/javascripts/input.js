// Didn't use OO
var dirMask = {
    'left':0b01,
    'right':0b10,
    'up':0b0100,
    'down':0b1000
}

var dirTranslate = [
    -1, // Don't translate
    0,  // Left
    180, // Right
    -1, // none
    90, // Top
    45, // Top Left
    135,    // Top Right
    -1, // none
    270,    // down
    315,    // Left Down
    225     // Right down
]

function deg2rad(deg){
    return deg * Math.PI / 180;
}

function rad2deg(rad){
    return rad * 180 / Math.PI;
}

function registerWSAD(scene){
    let wsad = {
                    up: scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
                  down: scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
                  left: scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
                 right: scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
                };
    return wsad;
}

function directionfromWSAD(wsad){
    let direction = 0;  // stop
    
    // get Direction
    // 5 4 6
    // 1 0 2
    // 9 8 10
    let lr = 0;
    let ud = 0;
    if(wsad.left.isDown){   // left takes precedence
        lr = 0b01;
    }else if(wsad.right.isDown){
        lr = 0b10;
    }
    
    if(wsad.up.isDown){ // up takes precedence
        ud = 0b01;
    }else if(wsad.down.isDown){
        ud = 0b10;
    }
    
    direction |= lr;
    direction |= (ud << 2);   // ud is 2 bits shifted left
    
    
    return direction;
}