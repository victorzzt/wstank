// New the object using new wsgame()
// Call the connect function ;made it separate, so that it can be called when needed, and can be disconnected manually
// 

class wsgame extends Object{
    constructor(){
        super();
    }
    
    connect(uri, log){
        // Events
        this.events = {};   // empty
        
        this.connected = false;
        this.logprefix = "[network.js:]"
        this.log = log;
        
        this.isconnected = false;
        
        let port = 80;
        if( location.protocal == 'https:' ){
            port = 433;
        }
        if( location.port != "" ){
            port = location.port;
        }
        
        this.connecturi = 'ws://' + location.hostname +":" + port + uri;
        
        this.initConnection(this.connecturi);
        this.reconnecttimer = setInterval(this.reconnect, 5000, this);    // 2 seconds reconnect
        
        this.maxpincount = 5;
        this.pingcount = this.maxpincount;
        this.pingtimer = setInterval(this.ping, 1000, this );
    }
    
    disconnect(){   // user disconnect
        if( this.connected ){
            this.ws.close();
        }
        clearInterval(this.reconnecttimer);
        clearInterval(this.pingtimer);
    }
    
    initConnection(){
        if(this.log){
            console.log(this.logprefix+ "Connect to:" + this.connecturi);
        }
        let wso = this;
        wso.ws = new WebSocket(this.connecturi, ["protocolOne", "protocolTwo"]);
        
        wso.ws.onopen = function(){
            console.log(wso.logprefix + "Connection opened");
            // fire client event
            wso.fireEvent('wsconn');
            
            wso.connected = true;
            wso.pingcount = wso.maxpincount;
        }
        wso.oneerror = function(e){
            // handle error
            // disable connection
            console.log(wso.logprefix + "Connection broken" + e.data);
            wso.connected = false;

            // fire client event
            wso.fireEvent('wserr');
        }
        wso.ws.onclose = function(){
            console.log(wso.logprefix + "Connection closed");
            // fire client event
            wso.connected = false;

            wso.fireEvent('wsclose');
        }
        
        // message
        wso.ws.onmessage = function(e){
            wso.procmsg(e, wso);
        }
    }
    
    reconnect(wso){
        console.log(wso.logprefix + "Alive:" + wso.connected);
        if( wso.connected ){
            return;
        }
        if(wso.log){
            console.log(wso.logprefix+ "Reconnect to:" + wso.connecturi);
        }
        wso.initConnection(wso.connecturi);
    }
    
    ping(wso){
        let now = (new Date()).getTime();
        // Send only if connected
        if( !wso.connected ){
            return;
        }
        wso.ws.send(JSON.stringify({'type':'ping','ts':now}));
        
        wso.pingcount--;   // countdown
        if(wso.pingcount <= 0)
        {
            console.log(wso.logprefix + "Connection Timeout");
            wso.ws.close();
        }
    }
    
    sendobj(obj){
        this.ws.send(JSON.stringify(obj));
    }
    
    sendtxt(txt){
        this.ws.send(txt);
    }
    
    procmsg(e, wso){
        var printError = function(error, explicit) {
            console.log(`[${explicit ? 'EXPLICIT' : 'INEXPLICIT'}] ${error.name}: ${error.message}`);
        }
        try{
            let rcev = JSON.parse(e.data);
            if(Array.isArray(rcev)){
                wso.fireEvent('rcevarr', rcev);
            }else if(rcev.type == 'pong'){
                wso.pingcount = wso.maxpincount;    // ping received
                let now = (new Date()).getTime();
                let tmping = now - rcev.ts;
                wso.fireEvent('ping',tmping);
            }else{
                wso.fireEvent('rcev',rcev); // whole packet
            }
        }catch(e){
            wso.fireEvent('rcevraw', e.data);   // if can't parse do the original
            if (e instanceof SyntaxError) {
                printError(e, true);
            } else {
                printError(e, false);
            }
        }
    }
    
    addEventListener(name, handler){
        handler.args = Array.prototype.splice.call(arguments, 2);
        if(this.events.hasOwnProperty(name)){
            this.events[name].push(handler);
        }else{
            this.events[name] = [handler];
        }
    }
    
    removeEventListener(name, handler){
        if(!this.events.hasOwnProperty(name)){
            return;
        }
        let index = this.events[name].indexOf(handler);
        if( index != -1 ){
            this.events[name].splice(index, 1);
        }
    }
    
    fireEvent(name){
        if(!this.events.hasOwnProperty(name)){
            return;
        }
        
        var evt = Array.prototype.splice.call(arguments, 1);
        
        if(!evt||!evt.length){
            evt = [];
        }
        
        let evs = this.events[name];
        let l = evs.length;
        for( let i = 0; i < l; i++){
            evs[i].apply(null,evt.concat(evs[i].args));
        }
    }
}