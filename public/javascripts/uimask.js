class uimask extends Phaser.GameObjects.Container{
    constructor(scene) {
        let children = [];

        // pausemask
        let pausemask = scene.add.graphics();
        pausemask.fillStyle(0xffffff, 1);
        pausemask.fillRect(0, 0, scene.game.renderer.width, scene.game.renderer.height);
        pausemask.setAlpha(0.5);
        
        let wifiico = scene.add.sprite(scene.game.renderer.width / 2, scene.game.renderer.height / 2, 'wfc').play('wfcrun');
        
        children.push(pausemask);
        children.push(wifiico);
        
        super(scene, 0, 0, children);
        
        this.setScrollFactor(0);
        this.scene = scene;
        
        scene.add.existing(this);
        
        this.pausemask = pausemask;
        this.wifiico = wifiico;
    }
    
    static loadsprites(scene){
        scene.load.animation('wfcani', 'assets/ui/wfcani.json');
        scene.load.atlas('wfc', 'assets/ui/wfc.png', 'assets/ui/wfc.json');
    }
    
    hide(){
        this.setVisible(false);
    }
    
    show(){
        this.setVisible(true);
    }
}