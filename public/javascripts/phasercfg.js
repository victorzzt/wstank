var phaserconfig = {
    type: Phaser.AUTO,
    width: 1024,
    height: 768,
    backgroundColor: '#000000',
    parent: 'phaser-game',
     physics: {
        default: 'matter',
        matter: {
            debug: true,
            gravity: {
                x: 0,
                y: 0
            },
            debugBodyColor: 0xffffff
        }
    },
    scene: MainScene
};