// Do not pass nested object into any field, not handled correctly.
class cookie extends Object{
    // static instance;     // seems not supported by firefox/edge
    // cookieobj = {};      // seems not supported by firefox/edge
    
    constructor(){
        if( cookie.instance == undefined ){
            super();
            this.cookieobj = {};
            cookie.instance = this;
1       }

        return cookie.instance;
    }
    
    read(){
        let cookiestr = document.cookie;
        this.cookieobj = new Object();
        
        let strlst = cookiestr.split(';');

        if( strlst[0] == ""){
            return; // nothing to read
        }
        if( strlst.length > 0 ){
            let that = this;
            let firstcookie = true;
            strlst.forEach( function (element){
                if( firstcookie ){
                    firstcookie = false;
                }else{
                    element = element.substr(1);    // somehow there's a spacebar
                }
                let cmd = element.split('=');
                that.cookieobj[cmd[0]] = cmd[1];
            } );
        }
    }
    
    write(obj){
        let cookie_str = "";
        for( let field in obj ){
            document.cookie = field + "=" + obj[field];
        }
        console.log(document.cookie);
    }
}
