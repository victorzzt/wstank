class bullet extends Phaser.GameObjects.Image{
    constructor(scene){
        super(scene, 0, 0, 'bullet');
        this.scene = scene; // record just incase
        this.maxlifespan = 400; // unit
        this.lifespan = 0;
        this.speed = Phaser.Math.GetSpeed(400,1);
        
        this.setData('type', 'bullet');
        
        // add physics body
        this.physicsContainer = scene.matter.add.gameObject(this);
        
        // Created died
        this.physicsContainer.setActive(false);
        this.visible(false);
    }
    
    static loadsprites(scene){  // class method (only need load once)
        scene.load.image('bullet', 'assets/tank/tankbullet.png');
    }
    
    fire(x, y, dir){
        
    }
    
    update(time, delta){
        
    }
    
    kill(){
        
    }
}