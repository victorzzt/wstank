class MainScene extends Phaser.Scene {

    constructor ()
    {
        super({ key: 'main' });
    }

    preload ()
    {
        var progress = this.add.graphics();
        this.load.on('progress', function (value) { // Load bar from : http://labs.phaser.io/edit.html?src=src/loader\loader%20events\load%20progress.js
            progress.clear();
            progress.fillStyle(0xffffff, 1);
            progress.fillRect(0, 270, 800 * value, 60);
        });
        this.load.on('complete', function () {
            progress.destroy();
        });
        
        // loadmap
        this.load.image("maptiles", "assets/map/maptiles.png");
        this.load.tilemapTiledJSON("map", "assets/map/mapfiles.json");
        
        // load tank
        uimask.loadsprites(this);
        tank.loadsprites(this);
    }

    create ()   // starting to have too much code, need to refactor
    {
        // Pause game
        // this.pause = !this.game.wso.connected;
        this.pause = true;
        
        // Display Ping
        this.pingtxt = this.add.text(10, 10, 'ping:', { font: '16px Courier', fill: '#ffffff' });
        this.pingtxt.setDepth(1000);
        this.pingtxt.setScrollFactor(0);
        
        // network Evt
        this.game.wso.addEventListener('ping',this.onPing,this);
        this.game.wso.addEventListener('rcev',this.onMsg, this);
        this.game.wso.addEventListener('rcevarr',this.onList, this);
        this.game.wso.addEventListener('wsconn',this.connected, this);
        this.game.wso.addEventListener('wsclose',this.disconnected, this);
        this.game.wso.addEventListener('wserr', this.disconnected, this);    // we only want to stop so close and err is same
        
        // Load the map
        this.map = this.make.tilemap({key: "map"});
        this.tileset = this.map.addTilesetImage("tiles01", "maptiles");
        
        this.groundLayer = this.map.createStaticLayer("GroundLayer", this.tileset, -64, -64);    // Using -64 is for the world wall
        this.wallLayer = this.map.createStaticLayer("WallLayer", this.tileset, -64, -64);
        this.wallLayer.setCollisionBetween(1, 16);
        this.matter.world.convertTilemapLayer(this.wallLayer);
        
        // need 2 categorys
        // this complex code is due to the lack of deactivating object in matter (or I'm not getting it right?) neither setActive nor killAndHide for pool work
        // object will be hidden but still operatable, anyone who found a more consistant way with Arcade physics would appreciate if you tell me.
        this.worldobj = 1;  // this is the default category, and the mask is collide with all
        this.activematter = this.matter.world.nextCategory();
        this.recyclematter = this.matter.world.nextCategory();
        
        this.matter.world.createDebugGraphic();
        // Add matter listener
        this.matter.world.on('collisionstart', this.onCollisionStart, this);
        
        // set world bounds
        this.worldx = 6336 ; // for easy adjustment (100 tiles x 6400 minus the edge)
        this.worldy = 6336 ;
        this.matter.world.setBounds(0, 0, this.worldx - 64, this.worldy - 64);  // should be tile size, shamelessly hard coded

        this.campadding = 64;
        // Of course camera , let's allow it out a bit
        this.cameras.main.setBounds(-this.campadding, -this.campadding, this.worldx + this.campadding, this.worldy + this.campadding);
        
        // add tanks group
        this.tanks = this.add.group();
        
        // add player
        // Get location
        let spawnloc = this.randomloc(this);
        this.player = new tank(this, spawnloc.x, spawnloc.y);
        this.player.setData('name', 'player');
        this.cameras.main.startFollow(this.player, true, 0.2, 0.2);
        
        this.tanks.add(this.player);
        // request player data from cookie
        this.cookie = new cookie();
                
        // register wsad
        this.wsad = registerWSAD(this);
        this.pointer = this.input.activePointer;
        
        // register uimask
        this.uimask = new uimask(this);
        if( this.pause ){
            this.uimask.show();
        }else{
            this.uimask.hide();
        }
        
        // this is my design error, network will connect before adding event listener, so need to invoke here
        if( this.game.wso.connected ){
            this.connected(this);
        }
    }
    
    update ()
    {
        // pause game
        if( this.pause ){
            return;
        }
        
        // player actions
        let playerdir = directionfromWSAD(this.wsad);
        this.player.run(playerdir);
        
        // Inefficient? maybe reuse the vector2 later
        let curCampos = new Phaser.Math.Vector2(this.cameras.main.scrollX , this.cameras.main.scrollY);     // Reason to do this is seems pointer's  worldx y not updating unless mouse is moved
        this.player.point(new Phaser.Math.Vector2(this.pointer.x + curCampos.x, this.pointer.y + curCampos.y));   // need to get world position
        this.playerupdate(this);
        
        // Enemy with dir needs to keep thrusting
        this.tanks.children.iterate(function(child){
            if( child.getData("name") == "enemy" ){
                child.run(child.mvdir);
            }
        });
    }
    
    // Ping
    onPing(ms, scene){
        let ping_val = ms;
        if( ping_val == -1 || ping_val == undefined){
            scene.pingtxt.setText('ping:inf');
        }else{
            scene.pingtxt.setText('ping:' + ping_val);
        }       
    }
    
    onMsg(msg, scene){
        if( msg.type == 'prpl' ){   // Player reply
            console.log("can start game, my ID is:" + msg.pid + ",expire:" + msg.expiry + ", Tint:", msg.tint);
            scene.pause = false;
            scene.uimask.hide();

            scene.player.setTint(msg.tint);  // from server
            scene.player.setData("pid", msg.pid);
            
            // save to cookie
            scene.cookie.write(msg);
        }
        
        if( msg.type == 'srvcfg'){
            console.log("Send Interval: " + msg.sync_interval);
            scene.sendInterval = msg.sync_interval; // sendqueue
            scene.sendqueue = [];   // empty the queue (Memory leak?)
            if( scene.sendtm ){
                // Use existing
                scene.sendtm.reset({ delay: scene.sendInterval, callback: scene.sendTheQueue, callbackScope: scene, loop: true });
            }else{
                scene.sendtm = scene.time.addEvent({ delay: scene.sendInterval, callback: scene.sendTheQueue, callbackScope: scene, loop: true });
            }
        }
                
        if( msg.type == 'pspw'){    // spawn enemy
            scene.spawnEnemy(msg, scene);
        }

        if( msg.type == 'pupd' ){   // update enemy
            scene.updateEnemy(msg, scene);
        }

        if( msg.type == 'pdel'){    // delete enemy
            scene.delEnemy(msg, scene);
        }
    }
    
    onList(lst,scene){  // process list
        // for now just back prop to onMsg
        lst.forEach(function(obj){
            scene.onMsg(obj, scene);
        });
    }
        
    // As matter only support global collision event
    // Possible https://photonstorm.github.io/phaser3-docs/Phaser.Physics.Matter.Sprite.html#addListener__anchor has add listener
    // However the use of it is vague
    // Another possibility is plugin: https://www.mikewesthad.com/phaser-matter-collision-plugin/docs/class/src/phaser-matter-collision-plugin.js~MatterCollisionPlugin.html
    // Seems the object can add itself to addOnCollideStart so now it's on scene level, which is bad for code reading
    onCollisionStart(event, bodyA, bodyB){
        // Context is scene, tested, event pair seems to be empty, don't know why
        /*  // legacy code
        if( bodyB.gameObject == this.player ){
            if( bodyA.gameObject != undefined){
                if( bodyA.gameObject.tile.layer.name == "WallLayer"){
                    console.log("Player hit Wall");
                }
            }
        }
        */
        // Seem in this context, bodyA will always be the static object
        if( bodyB.gameObject.getData('type') == 'tank' ){
            if( bodyB.gameObject.getData('name') == 'player' && bodyA.isStatic){
                // Legacy code
            }
        }
    }
    
    // the connection disconnect handlers
    connected(scene){
        console.log("sending request");
        scene.cookie.read();
        // console.log(scene.cookie.cookieobj);
        scene.playerobj = {type:"preq"};
        
        if( scene.cookie.cookieobj.pid ){
            scene.playerobj.pid = scene.cookie.cookieobj.pid;
        }else{
            scene.playerobj.pid = -1;
        }
        scene.playerobj.x = scene.player.x;
        scene.playerobj.y = scene.player.y;
        scene.playerobj.vx = scene.player.body.velocity.x;
        scene.playerobj.vy = scene.player.body.velocity.y;
        scene.playerobj.d = scene.player.mvdir;
        scene.playerobj.td = scene.player.turdeg;
        scene.game.wso.sendobj(scene.playerobj);
        
        scene.playerobj_last = scene.replicate(scene.playerobj);
    }
    
    disconnected(scene){
        scene.playerobj = undefined;    // delete the object
        scene.playerobj_last = undefined;
        scene.pause = true;
        scene.uimask.show();
        
        scene.tanks.children.iterate(function(child){   // or server reconnect will spawn additional one
            if(child.getData("name") == "enemy"){
                console.log("Enemy kill: " + msg.pid);
                child.die();
            }
        });
    }
    
    // Spawn an enemy
    spawnEnemy(msg, scene){
        let this_enemy = scene.tanks.getFirstDead();
        if( this_enemy == undefined ){
            console.log("Added tank to pool");
            this_enemy = new tank(scene, msg.x , msg.y);
            scene.tanks.add(this_enemy);
        }else{
            console.log("Got tank from pool");
        }
        // need to do anyway bc if revived needs to reposition
        this_enemy.x = msg.x;
        this_enemy.y = msg.y;
        this_enemy.setData("name","enemy");
        this_enemy.setData("pid",msg.pid);
        this_enemy.live();  // now is alive
        this_enemy.run(msg.d);
        this_enemy.pointang(msg.td);
        this_enemy.setTint(msg.tint);
        
        // log
        console.log("Enemy spawn: [id:" + msg.pid +", x:" + msg.x + ", y:" + msg.y + "]");
    }
    
    updateEnemy(msg, scene){
        if( msg.pid == this.player.getData("pid") ){    // self
            return;
        }
        
        scene.tanks.children.iterate(function(child){
           if( child.getData("pid") == msg.pid ){   // update state now only corrected when 
               if(msg.x != undefined) child.x = msg.x;
               if(msg.y != undefined) child.y = msg.y;
               // if(msg.vx != undefined) child.body.velocity.x = msg.vx;  // will vx vy work against thrust? let's test
               // if(msg.vy != undefined) child.body.velocity.y = msg.vy;
               if(msg.d != undefined) child.mvdir = msg.d;
               if(msg.td != undefined) {
                   child.turdeg = msg.td;
                   child.pointang(child.turdeg);
               }
           } 
        });
    }
    
    delEnemy(msg, scene){
        scene.tanks.children.iterate(function(child){
            if(child.getData("name") == "enemy" && child.getData("pid") == msg.pid){
                console.log("Enemy kill: " + msg.pid);
                child.die();
            }
        });
    }
    
    playerupdate(scene){
        if( scene.playerobj ){  // exist, or else don't do anything
            scene.playerobj.type = 'pupd';  // remember to push the type in before sending as it's not changing
            scene.playerobj_last.type = 'pupd';
            let playerchange = false;
            let sendobj = {};
            
            // get new data
            scene.playerobj.x = scene.player.x;
            scene.playerobj.y = scene.player.y;
            scene.playerobj.vx = scene.player.body.velocity.x;
            scene.playerobj.vy = scene.player.body.velocity.y;
            scene.playerobj.d = scene.player.mvdir;
            scene.playerobj.td = scene.player.turdeg;
            
            for( let field in scene.playerobj ){
                if( scene.playerobj[field] !=  "expiry"){   // expiry not relevant
                    if( scene.playerobj[field] != scene.playerobj_last[field]){   // firefox's simulation floating point never stop changing, 0.0001 should be sufficiently precise
                        playerchange = true;
                        scene.playerobj_last[field] = scene.playerobj[field];   // update the old object
                        sendobj[field] = scene.playerobj[field];
                    }
                }
            }
            
            sendobj.type = 'pupd';
            sendobj.pid = this.player.getData("pid");
            if(playerchange){
                let alreadyinsq = this.searchplayers(scene, scene.player.getData("pid"));
                if(alreadyinsq.length > 0){ // exist
                    if(alreadyinsq.length > 1){ // why do we have more than one
                        console.log("Warning: Duplicate in send queue");
                    }
                    alreadyinsq.forEach(function(obj){  // update values
                        for( let field in sendobj ){
                            obj[field] = sendobj[field];    // only update the ones that exist
                        }
                    });
                }else{
                    scene.sendqueue.push( sendobj );    // push into queue
                }
            }
        }
    }
    
    // Send the queue
    sendTheQueue(){
        let scene = this;   // somehow callback scope works
        if( scene.sendqueue ){
            if( Array.isArray(scene.sendqueue) ){   // protectors
                if( scene.sendqueue.length > 0 ){   // has item
                    scene.game.wso.sendobj(scene.sendqueue);
                    scene.sendqueue = [];   // memory leak?
                }
            }
        }
    }
    
    // no requesting, just generate locally (seems unlikely to overlap with other player)
    randomloc(scene){
        let padding = 64;   // safe distance
        let posxmax = this.worldx - 2 * padding;    // including the wall
        let posymax = this.worldy - 2 * padding;
        
        let posx = 0;
        let posy = 0;
        
        let reject;
        do{
            reject = false;
            posx = Math.round( Math.random() * (posxmax - padding) + padding );
            posy = Math.round( Math.random() * (posymax - padding) + padding );
            
            for( let i = 0; i < 4; i++ ){   // detect 4 corners
                let detx = (i & 0x01)? posx + padding / 2 : posx - padding / 2;
                let dety = (i & 0x02)? posy + padding / 2 : posy - padding / 2;
                if(scene.wallLayer.getTileAtWorldXY(detx,dety) != undefined){
                    reject = true;
                }
            }
        }while(reject);
        
        return new Phaser.Math.Vector2(posx, posy);
    }
    
    // replicate an object
    replicate(obj){
        return JSON.parse(JSON.stringify(obj));
    }
    
    // search for queue if send obj exist
    searchplayers(scene, pid){
        let result = [];    // nothing
        scene.sendqueue.forEach(function(obj){
            if( obj ){
                if( obj.pid == pid ){
                    result.push(obj);   // yes we do need to send the reference
                }
            }
        });
        
        return result;
    }
}