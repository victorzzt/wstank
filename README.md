# Ping Pong
#### *This is a rough implementation of the Ping Pong of websocket nodejs browser comm*

* If you have just cloned the library, before start, you'll need to run **npm install**
* The server websocket code is still inside *bin/www* which will need to update later
  * It can be updated to a module and embed into app.js in root folder
* The main code that connects to the server is in *public/javascripts/network.js* (which should be reusable)
  * The one that's using the class is inside *public/javascripts/mainscene.js*
  * Inside *public/javascripts/main.js*, the object is instantiated and added to *game* object for easy access in any scenes.
  
  * Currently different player can get different color
  * Reject dual connect using ID
  
  * Known problem: currently there's no one player that have dominant state of physics, pushing seems to have problem when lagging.
  
  * Roadmap:
    * Update all player position on connect (this will lead to newly connected player unable to see bullet location)
    * Instant send when there's input change
    * Push to minor send queue to send every 0.1s
    * Refactor server side code to be better understandable
	* Code refactoring ( as current code already unreadable )
	* Browser speed difference ( this is more like how to work around how matterJS engine deals with browser time )